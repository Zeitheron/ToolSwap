package com.zeitheron.toolswap.api;

import java.util.HashMap;
import java.util.Map;

import com.zeitheron.toolswap.ToolSwapMod;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.common.MinecraftForge;

public class BlockCollection
{
	private static final Map<Block, Item> ITEM_MAP = new HashMap<Block, Item>();
	private static final Map<Block, String> TOOL_MAP = new HashMap<Block, String>();
	
	public static void putItemMapping(Item tool, Block target)
	{
		ITEM_MAP.put(target, tool);
	}
	
	public static void putToolMapping(String tool, Block target)
	{
		TOOL_MAP.put(target, tool);
	}
	
	public static Item getItemMapping(Block block)
	{
		return ITEM_MAP.get(block);
	}
	
	public static String getToolMapping(Block block)
	{
		return TOOL_MAP.get(block);
	}
	
	public static void reload()
	{
		TOOL_MAP.clear();
		ITEM_MAP.clear();
		ToolSwapMod.reloadBC();
		MinecraftForge.EVENT_BUS.post(new TSToolReloadEvent());
	}
}