package com.zeitheron.toolswap.api;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;

public class PredefinedSets
{
	public static int getSlotForBlock(EntityPlayer player, BlockPos pos, IBlockState state)
	{
		if(player == null || state == null)
			return -1;
		
		InventoryPlayer p = player.inventory;
		
		Map<Integer, Integer> harvestLevels = new HashMap<Integer, Integer>();
		
		for(int i = 0; i < 9; ++i)
		{
			ItemStack stack = p.getStackInSlot(i);
			if(stack == null)
				continue;
			
			Set<String> tools = stack.getItem().getToolClasses(stack);
			for(String tool : tools.toArray(new String[0]))
			{
				int hl = stack.getItem().getHarvestLevel(stack, tool, player, state);
				if(state.getBlock().isToolEffective(tool, state))
					harvestLevels.put(i, harvestLevels.getOrDefault(i, 0) + hl);
			}
		}
		
		int maxS = -1;
		int maxHL = Integer.MIN_VALUE;
		for(Integer slot : harvestLevels.keySet().toArray(new Integer[0]))
		{
			if(slot == null || harvestLevels.get(slot) == null)
				continue;
			int hl = harvestLevels.get(slot).intValue();
			
			if(hl > maxHL)
			{
				maxHL = hl;
				maxS = slot.intValue();
			}
		}
		
		if(maxS == -1)
		{
			harvestLevels.clear();
			
			for(int i = 0; i < 9; ++i)
			{
				ItemStack stack = p.getStackInSlot(i);
				if(stack == null)
					continue;
				Set<String> tools = stack.getItem().getToolClasses(stack);
				String tool = BlockCollection.getToolMapping(state.getBlock());
				Item itemTool = BlockCollection.getItemMapping(state.getBlock());
				
				if(tool != null && tools.contains(tool))
					harvestLevels.put(i, harvestLevels.getOrDefault(i, 0) + stack.getItem().getHarvestLevel(stack, tool, player, state));
				else if(itemTool != null && stack.getItem() == itemTool)
					harvestLevels.put(i, harvestLevels.getOrDefault(i, 0) + stack.getItem().getHarvestLevel(stack, tool, player, state));
			}
			
			maxS = -1;
			maxHL = Integer.MIN_VALUE;
			for(Integer slot : harvestLevels.keySet().toArray(new Integer[0]))
			{
				if(slot == null || harvestLevels.get(slot) == null)
					continue;
				int hl = harvestLevels.get(slot).intValue();
				
				if(hl > maxHL)
				{
					maxHL = hl;
					maxS = slot.intValue();
				}
			}
		}
		
		return maxS;
	}
}