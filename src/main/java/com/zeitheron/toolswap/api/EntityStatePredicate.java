package com.zeitheron.toolswap.api;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;
import java.util.function.Supplier;

import com.zeitheron.hammercore.lib.zlib.tuple.TwoTuple;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.passive.EntitySheep;
import net.minecraft.util.math.Vec3d;

public class EntityStatePredicate
{
	public static final Map<String, Predicate<Entity>> PREDICATES = new HashMap<>();
	static
	{
		bindWithOpposite("child", "adult", ent ->
		{
			if(ent instanceof EntityAgeable)
				return ((EntityAgeable) ent).isChild();
			return false;
		});
	}
	
	public static void bindWithOpposite(String name, String opposite, Predicate<Entity> pred)
	{
		bind(name, pred);
		bind(opposite, ent -> !test(name, ent));
	}
	
	public static void bind(String name, Predicate<Entity> pred)
	{
		Predicate<Entity> p = PREDICATES.get(name);
		if(p == null)
			p = pred;
		else
			p = p.or(pred);
		PREDICATES.put(name, p);
	}
	
	public static boolean testPredicate(String name, Entity ent, Supplier<Vec3d> pos)
	{
		Predicate<Entity> pred = null;
		// if(!name.isEmpty())
		// {
		// TwoTuple<Integer, Character> fo = findFirst(name, '&', '|');
		// while(fo.get1().intValue() >= 0)
		// {
		// String token = name.substring(0, fo.get1().intValue());
		// char f = fo.get2().charValue();
		// Predicate<Entity> p = PREDICATES.getOrDefault(name,
		// compoundPredicate(token, pos));
		// pred = pred == null ? p : (f == '|' ? pred.or(p) : pred.and(p));
		// name = name.substring(fo.get1().intValue() + 1);
		// if(name.isEmpty())
		// break;
		// fo = findFirst(name, '&', '|');
		// }
		// }
		
		if(name.isEmpty())
			return true;
		
		name = "&" + name;
		while(!name.isEmpty())
		{
			char f = name.isEmpty() ? '&' : name.charAt(0);
			name = name.substring(1);
			String token = "";
			while(!name.isEmpty() && name.charAt(0) != '&' && name.charAt(0) != '|')
			{
				token += name.charAt(0);
				name = name.substring(1);
			}
			
			Predicate<Entity> p = PREDICATES.getOrDefault(token, compoundPredicate(token, pos));
			pred = pred == null ? p : (f == '|' ? pred.or(p) : pred.and(p));
		}
		
		return pred == null || pred.test(ent);
	}
	
	public static Predicate<Entity> compoundPredicate(String str, Supplier<Vec3d> pos)
	{
		if(str.startsWith("distance"))
		{
			String sub = str.substring(8);
			if(sub.startsWith(">="))
			{
				double num = Double.parseDouble(sub.substring(2));
				return e -> pos.get().distanceTo(e.getPositionVector()) >= num;
			} else if(sub.startsWith("<="))
			{
				double num = Double.parseDouble(sub.substring(2));
				return e -> pos.get().distanceTo(e.getPositionVector()) <= num;
			} else if(sub.startsWith(">"))
			{
				double num = Double.parseDouble(sub.substring(1));
				return e -> pos.get().distanceTo(e.getPositionVector()) > num;
			} else if(sub.startsWith("<"))
			{
				double num = Double.parseDouble(sub.substring(1));
				return e -> pos.get().distanceTo(e.getPositionVector()) < num;
			}
		} else if(str.startsWith("color="))
		{
			String color = str.substring(6);
			return e -> e instanceof EntitySheep && ((EntitySheep) e).getFleeceColor().getDyeColorName().equalsIgnoreCase(color);
		}
		return e -> false;
	}
	
	public static boolean test(String name, Entity ent)
	{
		Predicate<Entity> p = PREDICATES.get(name);
		return p == null ? false : p.test(ent);
	}
	
	private static TwoTuple<Integer, Character> findFirst(String str, char... cs)
	{
		int index = str.length() * 2;
		char fo = ' ';
		for(char c : cs)
		{
			int i = str.indexOf(c);
			if(i != -1 && index > i)
			{
				index = i;
				fo = c;
			}
		}
		return new TwoTuple<Integer, Character>(index > str.length() ? -1 : index, fo);
	}
}