package com.zeitheron.toolswap.api;

import com.zeitheron.hammercore.lib.zlib.utils.MD5;

import net.minecraft.item.ItemStack;

public class StackMD5
{
	public static String of(ItemStack stack, boolean includeDamage)
	{
		return MD5.encrypt(stack.getItem().getRegistryName().toString() + stack.getCount() + stack.getTagCompound() + (includeDamage ? stack.getItemDamage() : ""));
	}
}