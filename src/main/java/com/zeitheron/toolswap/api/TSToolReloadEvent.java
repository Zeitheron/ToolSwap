package com.zeitheron.toolswap.api;

import net.minecraftforge.fml.common.eventhandler.Event;

/**
 * Called when the {@link BlockCollection} is being reloaded.
 */
public class TSToolReloadEvent extends Event
{
}