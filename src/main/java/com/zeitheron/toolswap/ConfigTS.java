package com.zeitheron.toolswap;

import com.zeitheron.hammercore.cfg.HCModConfigurations;
import com.zeitheron.hammercore.cfg.IConfigReloadListener;
import com.zeitheron.hammercore.cfg.fields.ModConfigPropertyInt;

@HCModConfigurations(modid = "toolswap")
public class ConfigTS implements IConfigReloadListener
{
	@ModConfigPropertyInt(category = "Swapping", name = "SwapDelay", min = 3, max = 40, defaultValue = 7, comment = "How many ticks will pass after you're done mining till the slot selected reverts back to original (before mining)")
	public static int swapDelay = 7;
}