package com.zeitheron.toolswap;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.client.render.item.ItemRenderingHandler;
import com.zeitheron.hammercore.lib.zlib.utils.Threading;
import com.zeitheron.toolswap.api.BlockCollection;
import com.zeitheron.toolswap.api.PredefinedSets;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.BreakSpeed;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.ClientTickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.common.network.FMLNetworkEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class SwapEvents
{
	public static final Predicate<EntityPlayer> CLIENT_PLAYER = pl -> Objects.equals(pl.getGameProfile().getId(), HammerCore.renderProxy.getClientPlayer().getGameProfile().getId());
	
	public static boolean shouldSwap = false;
	public static int swapTime = 0;
	public static IntList slotHistory = new IntArrayList();
	
	public final File configFile = new File(Loader.instance().getConfigDir(), "toolswap.dat");
	
	public SwapEvents bindItemRendering()
	{
		for(Item it : GameRegistry.findRegistry(Item.class).getValuesCollection())
			if(it instanceof ItemTool)
				ItemRenderingHandler.INSTANCE.appendItemRender(it, RenderItemHook.TOOL_HOOK);
		return this;
	}
	
	@SubscribeEvent
	public void clientTick(ClientTickEvent e)
	{
		// Skip end phase
		if(e.phase == Phase.END)
			return;
		
		if(swapTime == 0 && slotHistory.size() > 0)
		{
			Minecraft.getMinecraft().player.inventory.currentItem = slotHistory.get(0).intValue();
			slotHistory.clear();
		}
		if(swapTime > 0)
			--swapTime;
	}
	
	@SubscribeEvent
	public void attackEntity(AttackEntityEvent e)
	{
		EntityPlayer player = e.getEntityPlayer();
		
		if(!shouldSwap || player == null || !CLIENT_PLAYER.test(player))
			return;
		
		InventoryPlayer p = player.inventory;
		
		Map<Integer, ItemSword> swords = new HashMap<Integer, ItemSword>();
		
		for(int i = 0; i < 9; ++i)
		{
			ItemStack stack = p.getStackInSlot(i);
			if(stack == null)
				continue;
			if(stack.getItem() instanceof ItemSword)
				swords.put(i, (ItemSword) stack.getItem());
		}
		
		int maxS = -1;
		float maxHL = Integer.MIN_VALUE;
		for(Integer slot : swords.keySet().toArray(new Integer[0]))
		{
			if(slot == null || swords.get(slot) == null)
				continue;
			float hl = ToolMaterial.valueOf(swords.get(slot).getToolMaterialName()).getDamageVsEntity();
			if(hl > maxHL)
			{
				maxHL = hl;
				maxS = slot.intValue();
			}
		}
		
		if(maxS != -1)
			player.inventory.currentItem = maxS;
	}
	
	@SubscribeEvent
	public void breakSpeed(BreakSpeed e)
	{
		EntityPlayer player = e.getEntityPlayer();
		
		if(!shouldSwap || player == null || !CLIENT_PLAYER.test(player))
			return;
		
		int maxS = -1;
		
		try
		{
			maxS = PredefinedSets.getSlotForBlock(player, e.getPos(), e.getState());
		} catch(Throwable err)
		{
		}
		
		if(maxS != -1)
		{
			int prev = player.inventory.currentItem;
			player.inventory.currentItem = maxS;
			boolean c = e.getState().getBlock().canHarvestBlock(player.getEntityWorld(), e.getPos(), player);
			player.inventory.currentItem = prev;
			
			if(c)
			{
				if(player.inventory.currentItem != maxS)
					slotHistory.add(player.inventory.currentItem);
				player.inventory.currentItem = maxS;
				
				setSwapped();
			}
		}
	}
	
	@SubscribeEvent
	public void clientDisconnect(FMLNetworkEvent.ClientDisconnectionFromServerEvent e)
	{
		Minecraft.getMinecraft().addScheduledTask(this::saveOptions);
	}
	
	@SubscribeEvent
	public void clientConnect(FMLNetworkEvent.ClientConnectedToServerEvent e)
	{
		Minecraft.getMinecraft().addScheduledTask(this::loadOptions);
		Threading.createAndStart("ToolSwapReloaderThread", BlockCollection::reload);
	}
	
	public void setSwapped()
	{
		swapTime = ConfigTS.swapDelay;
	}
	
	public void saveOptions()
	{
		try(DataOutputStream o = new DataOutputStream(new GZIPOutputStream(new FileOutputStream(configFile))))
		{
			o.writeBoolean(shouldSwap);
		} catch(IOException ioe)
		{
			ToolSwapMod.LOG.error("Failed to save configs!", ioe);
		}
	}
	
	public void loadOptions()
	{
		if(configFile.isFile())
			try(DataInputStream i = new DataInputStream(new GZIPInputStream(new FileInputStream(configFile))))
			{
				shouldSwap = i.readBoolean();
			} catch(IOException ioe)
			{
				ToolSwapMod.LOG.error("Failed to load configs!", ioe);
			}
		else
		{
			shouldSwap = true;
		}
	}
	
	public static boolean isFav(ItemStack stack)
	{
		return !stack.isEmpty() && stack.hasTagCompound() && stack.getTagCompound().getBoolean("TSFav");
	}
	
	public static void toggleFav(ItemStack stack)
	{
		NBTTagCompound nbt = stack.hasTagCompound() ? stack.getTagCompound() : new NBTTagCompound();
		stack.setTagCompound(nbt);
		nbt.setBoolean("TSFav", !nbt.getBoolean("TSFav"));
	}
}