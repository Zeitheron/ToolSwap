package com.zeitheron.toolswap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.zeitheron.toolswap.api.EntityStatePredicate;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.event.ClickEvent;

public class CommandTSwap extends CommandBase
{
	@Override
	public int getRequiredPermissionLevel()
	{
		return 0;
	}
	
	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		return sender instanceof EntityPlayer;
	}
	
	@Override
	public String getName()
	{
		return "tswap";
	}
	
	@Override
	public String getUsage(ICommandSender sender)
	{
		return "Use /tswap";
	}
	
	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		if(args.length == 0)
			throw new CommandException("No subcommand passed!");
		
		if(args[0].equalsIgnoreCase("toggle"))
		{
			SwapEvents.shouldSwap = !SwapEvents.shouldSwap;
			sender.sendMessage(new TextComponentString("[").appendText(TextFormatting.AQUA + "ToolSwap").appendText("] Tool Swapping is now ").appendText(SwapEvents.shouldSwap ? TextFormatting.GREEN + "enabled" : TextFormatting.RED + "disabled").appendText("."));
		} else /*if(args[0].equalsIgnoreCase("togglefav"))
		{
			EntityPlayer player = (EntityPlayer) sender;
			ItemStack st = player.inventory.getCurrentItem();
			if(!st.isEmpty() && st.getItem() instanceof ItemTool)
			{
				SwapEvents.toggleFav(st);
				player.inventory.setInventorySlotContents(player.inventory.currentItem, st.copy());
				HCNetwork.c_sendToServer(new CPacketCreativeInventoryAction(player.inventory.currentItem, st.copy()));
			}
		} else*/ if(args[0].equalsIgnoreCase("countentity"))
		{
			if(args.length < 2)
				throw new CommandException("No entity passed!");
			
			Class<? extends Entity> entClass = EntityList.getClassFromName(args[1]);
			
			if(args.length == 2)
				throw new CommandException("Distance not passed!");
			
			double dist = parseDouble(args[2]);
			
			int fo = sender.getEntityWorld().getEntitiesWithinAABB(entClass, new AxisAlignedBB(sender.getPositionVector(), sender.getPositionVector()).grow(dist), ent -> EntityStatePredicate.testPredicate(args.length >= 4 ? args[3] : "", ent, sender::getPositionVector)).size();
			
			sender.sendMessage(new TextComponentString("Found " + fo + " " + EntityList.getTranslationName(EntityList.getKey(entClass))));
		} else if(args[0].equalsIgnoreCase("help") || args[0].equals("?"))
		{
			if(args.length == 1)
			{
				sender.sendMessage(new TextComponentString("Here are valid subcommands:"));
				
				TextComponentString txt = new TextComponentString("toggle");
				txt.getStyle().setUnderlined(true);
				txt.getStyle().setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/tswap toggle"));
				txt.getStyle().setColor(TextFormatting.AQUA);
				sender.sendMessage(new TextComponentString("* ").appendSibling(txt).appendText(" toggles the Tool Swapping feature."));
				
				txt = new TextComponentString("togglefav");
				txt.getStyle().setUnderlined(true);
				txt.getStyle().setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/tswap togglefav"));
				txt.getStyle().setColor(TextFormatting.AQUA);
				sender.sendMessage(new TextComponentString("* ").appendSibling(txt).appendText(" toggles the selected item in main hand as '").appendText(TextFormatting.YELLOW + "favorite").appendText("' tool."));
				
				txt = new TextComponentString("countentity <entity> <radius> [search params]");
				txt.getStyle().setUnderlined(true);
				txt.getStyle().setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/tswap countentity <entity> <radius> [search params]"));
				txt.getStyle().setColor(TextFormatting.AQUA);
				sender.sendMessage(new TextComponentString("* ").appendSibling(txt).appendText(". Search params is a compound split by either '&' (AND) or '|' (OR). Example: color=yellow&adult - When applying to sheep, will count only those who are adult and their fleece color is yellow. (This command has well-supported tab helping)"));
			}
		}
	}
	
	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos targetPos)
	{
		if(args.length == 1)
			return getListOfStringsMatchingLastWord(args, "toggle", "countentity", /*"togglefav",*/ "help", "?");
		if(args.length == 2 && args[0].equalsIgnoreCase("countentity"))
			return getListOfStringsMatchingLastWord(args, EntityList.getEntityNameList());
		if(args.length == 4 && args[0].equalsIgnoreCase("countentity"))
		{
			List<String> tabs = new ArrayList<>();
			tabs.addAll(EntityStatePredicate.PREDICATES.keySet());
			for(EnumDyeColor c : EnumDyeColor.values())
				tabs.add("color=" + c.getDyeColorName().toLowerCase());
			tabs.add("distance>=");
			tabs.add("distance>");
			tabs.add("distance<=");
			tabs.add("distance<");
			
			List<String> ret = new ArrayList<>();
			
			if(!args[3].contains("&") && !args[3].contains("|"))
				ret.addAll(tabs);
			else
			{
				String[] splitItA = args[3].split("[&]");
				List<String> splits = new ArrayList<>();
				for(String s : splitItA)
					splits.addAll(Arrays.asList(s.split("[|]")));
				boolean suggestNext = args[3].endsWith("&") || args[3].endsWith("|");
				
				if(suggestNext)
					tabs.stream().filter(c -> !splits.contains(c)).forEach(s -> ret.add(args[3] + s));
				else
				{
					StringBuilder bef = new StringBuilder();
					int and = args[3].lastIndexOf("&");
					int or = args[3].lastIndexOf("|");
					int sub = or != -1 && or > and ? or : and;
					if(sub != -1)
						bef.append(args[3].substring(0, sub + 1));
					
					List<String> ok = getListOfStringsMatchingLastWord(new String[] { splits.get(splits.size() - 1) }, tabs);
					ok.stream().filter(c -> !splits.contains(c)).forEach(s -> ret.add(bef + s));
					System.out.println(or + " " + and + " |" + bef);
				}
			}
			
			return getListOfStringsMatchingLastWord(args, ret);
		}
		return super.getTabCompletions(server, sender, args, targetPos);
	}
}