package com.zeitheron.toolswap;

import com.zeitheron.hammercore.client.render.item.IItemRender;
import com.zeitheron.hammercore.client.utils.UV;

import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms.TransformType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class RenderItemHook implements IItemRender
{
	public static final RenderItemHook TOOL_HOOK = new RenderItemHook();
	
	public final UV star = new UV(new ResourceLocation("toolswap", "textures/star.png"), 0, 0, 256, 256);
	
	@Override
	public void renderItem(ItemStack item)
	{
	}
	
	@Override
	public void renderItem(ItemStack stack, IBakedModel bakedmodel, TransformType transform)
	{
		if(transform == TransformType.GUI)
			renderGui(stack);
	}
	
	public void renderGui(ItemStack stack)
	{
		if(SwapEvents.isFav(stack))
			star.render(1, 1, 6, 6);
	}
}