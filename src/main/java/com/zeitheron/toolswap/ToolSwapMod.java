package com.zeitheron.toolswap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.lib.zlib.error.JSONException;
import com.zeitheron.hammercore.lib.zlib.io.IOUtils;
import com.zeitheron.hammercore.lib.zlib.json.JSONArray;
import com.zeitheron.hammercore.lib.zlib.json.JSONObject;
import com.zeitheron.hammercore.lib.zlib.json.JSONTokener;
import com.zeitheron.hammercore.lib.zlib.utils.Joiner;
import com.zeitheron.toolswap.api.BlockCollection;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.ModMetadata;
import net.minecraftforge.fml.common.event.FMLFingerprintViolationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.registries.IForgeRegistry;

@Mod(modid = "toolswap", name = "Tool Swap", version = "@VERSION@", dependencies = "required-after:hammercore", certificateFingerprint = "4d7b29cd19124e986da685107d16ce4b49bc0a97", clientSideOnly = true)
public class ToolSwapMod
{
	public static SwapEvents events;
	public static final Logger LOG = LogManager.getLogger("Tool Swap");
	
	@EventHandler
	public void certificateViolation(FMLFingerprintViolationEvent e)
	{
		LOG.warn("*****************************");
		LOG.warn("WARNING: Somebody has been tampering with ToolSwap jar!");
		LOG.warn("It is highly recommended that you redownload mod from https://minecraft.curseforge.com/projects/246309 !");
		LOG.warn("*****************************");
		HammerCore.invalidCertificates.put("toolswap", "https://minecraft.curseforge.com/projects/246309");
	}
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent e)
	{
		ModMetadata meta = e.getModMetadata();
		meta.autogenerated = false;
		meta.authorList = HammerCore.getHCAuthorsArray();
		meta.version = "@VERSION@";
		MinecraftForge.EVENT_BUS.register(events = new SwapEvents());
		ClientCommandHandler.instance.registerCommand(new CommandTSwap());
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent e)
	{
		events.bindItemRendering();
	}
	
	public static void reloadBC()
	{
		LOG.info("Reloading built-in overrides...");
		
		long start = System.currentTimeMillis();
		
		/** Contains all overrides from configs, internal AND pastebin */
		JSONObject obj = loadOverrides();
		
		JSONObject items = obj.optJSONObject("items");
		JSONObject tools = obj.optJSONObject("tools");
		
		IForgeRegistry<Item> itemReg = GameRegistry.findRegistry(Item.class);
		IForgeRegistry<Block> BlockReg = GameRegistry.findRegistry(Block.class);
		
		if(items != null)
			for(String item : items.keySet())
			{
				Item it = itemReg.getValue(new ResourceLocation(item));
				if(it != null)
				{
					JSONArray arr = items.optJSONArray(item);
					int s = arr.length();
					for(int i = 0; i < s; ++i)
					{
						String str = arr.optString(i);
						Block bl = str != null ? BlockReg.getValue(new ResourceLocation(str)) : null;
						if(bl != null)
							BlockCollection.putItemMapping(it, bl);
					}
				}
			}
		
		if(tools != null)
			for(String tool : tools.keySet())
				if(tool != null)
				{
					JSONArray arr = tools.optJSONArray(tool);
					int s = arr.length();
					for(int i = 0; i < s; ++i)
					{
						String str = arr.optString(i);
						Block bl = str != null ? BlockReg.getValue(new ResourceLocation(str)) : null;
						if(bl != null)
							BlockCollection.putToolMapping(tool, bl);
					}
				}
			
		long end = System.currentTimeMillis();
		LOG.info("  Reloaded in " + (end - start) + " ms.");
	}
	
	public static JSONObject loadOverrides()
	{
		JSONObject jo = null;
		try
		{
			jo = (JSONObject) new JSONTokener(new String(IOUtils.pipeOut(ToolSwapMod.class.getResourceAsStream("/tswap_overrides.json")))).nextValue();
		} catch(Throwable err)
		{
			err.printStackTrace();
		}
		
		File overrides = new File(Loader.instance().getConfigDir(), "toolswap_overrides.json");
		if(!overrides.isFile())
			try(OutputStream os = new FileOutputStream(overrides))
			{
				os.write(IOUtils.pipeOut(ToolSwapMod.class.getResourceAsStream("/tswap_overrides.json")));
			} catch(IOException ioe)
			{
				ioe.printStackTrace();
			}
		try
		{
			jo = merge(jo, (JSONObject) new JSONTokener(Joiner.NEW_LINE.join(Files.readAllLines(overrides.toPath()))).nextValue());
		} catch(Throwable err)
		{
			err.printStackTrace();
		}
		try
		{
			jo = merge(jo, (JSONObject) IOUtils.downloadjson("https://pastebin.com/raw/92QrP2VF"));
		} catch(Throwable err)
		{
			err.printStackTrace();
		}
		
		return jo;
	}
	
	private static JSONArray merge(JSONArray a, JSONArray b)
	{
		if(a == b && a == null)
			return null;
		if(a == null || b == null)
			return a != null ? a : b;
		
		Set<Object> set = new HashSet<>();
		for(Object av : a.values())
			set.add(av);
		for(Object bv : b.values())
			set.add(bv);
		
		JSONArray arr = new JSONArray();
		for(Object sv : set)
			arr.put(sv);
		return arr;
	}
	
	private static JSONObject merge(JSONObject a, JSONObject b)
	{
		if(a == b && a == null)
			return null;
		if(a == null || b == null)
			return a != null ? a : b;
		JSONObject obj = new JSONObject();
		
		try
		{
			for(String keyA : a.keySet())
				obj.putOpt(keyA, a.get(keyA));
		} catch(JSONException jsonex)
		{
		}
		
		try
		{
			for(String keyB : b.keySet())
			{
				Object ins = obj.opt(keyB);
				Object bv = b.get(keyB);
				
				// Handle compound merging
				if(ins != null && bv != null)
				{
					if(ins instanceof JSONObject && bv instanceof JSONObject)
						bv = merge((JSONObject) ins, (JSONObject) bv);
					if(ins instanceof JSONArray && bv instanceof JSONArray)
						bv = merge((JSONArray) ins, (JSONArray) bv);
				}
				
				obj.putOpt(keyB, bv);
			}
		} catch(JSONException jsonex)
		{
		}
		
		return obj;
	}
}